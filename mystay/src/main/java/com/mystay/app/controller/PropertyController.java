package com.mystay.app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.mystay.app.dto.ApiResponse;
import com.mystay.app.entities.Property;
import com.mystay.app.enums.PropertyAvailablity;
import com.mystay.app.enums.PropertyStatus;
import com.mystay.app.service.PropertyService;
import com.mystay.app.service.UserService;


import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/property")
@RequiredArgsConstructor
public class PropertyController {
	
	@Autowired
	private PropertyService propertyService;
	
	@Autowired
	private UserService userService;
	
	// Add Property
	@PostMapping("/addproperty")
	public ResponseEntity<ApiResponse<String>> addProperty(@RequestBody Property property) {
		try {
			// Logic to add current Property Owner 
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        String username;
	        if (principal instanceof UserDetails) {
	            username = ((UserDetails) principal).getUsername();
	        } else {
	            username = principal.toString();
	        }

	        property.setOwnerName(username);
			System.out.println("Token : " + username); 
			property.setStatus(PropertyStatus.VERIFICATION_PENDING);
			property.setAvailability(PropertyAvailablity.AVAILABLE);
			
			propertyService.saveProperty(property);
            
            ApiResponse<String> response;
    		response = new ApiResponse<>("Success", "Property added successfully");
        	return new ResponseEntity<>(response, HttpStatus.OK);
        	
		} catch (Exception e) {
			ApiResponse<String> response;
    		response = new ApiResponse<>("error", e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } 
	}
	
	// Change Property Availability
	@PostMapping("/changeAvail/{id}")
	public ResponseEntity<ApiResponse<String>> changePropertyAvailabilityToRented(@RequestParam("id") Integer id) {
		Optional<Property> property = propertyService.getPropertyById(id);
		Property p = property.get();
		
		if(property.isPresent()) {
			if(p.getAvailability() == PropertyAvailablity.AVAILABLE) {
				p.setAvailability(PropertyAvailablity.RENTED);
			} else {
				p.setAvailability(PropertyAvailablity.AVAILABLE);
			}
			propertyService.saveProperty(p);			
			ApiResponse<String> response;
			response = new ApiResponse<>("Success", "Property Status changed to " + p.getAvailability());
	    	return new ResponseEntity<>(response, HttpStatus.OK);
			
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Property not found with id: " + id);
        }
	}
	
	@GetMapping("/propertyId/{id}")
    public ResponseEntity<ApiResponse<Property>> getPropertyById(@PathVariable("id") Integer id) {
        Optional<Property> property = propertyService.getPropertyById(id);
        
        
        ApiResponse<Property> response;                
        if (property.isPresent()) {
        	response = new ApiResponse<>("Success", property.get());
        	return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Property not found with id: " + id);
        }
    }
	
	
	// Get All owners Properties
	@GetMapping("/allProperties")
	public ResponseEntity<ApiResponse<Optional<List<Property>>>> getAllProperties() {
		// Logic to add current Property Owner 
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    String username;
	    if (principal instanceof UserDetails) {
	        username = ((UserDetails) principal).getUsername();
	    } else {
	        username = principal.toString();
	    }
		Optional<List<Property>> properties = propertyService.getAllOwnersProperties(username);
		
		ApiResponse<Optional<List<Property>>> response;
		response = new ApiResponse<>("Success", properties);
    	return new ResponseEntity<>(response, HttpStatus.OK);	
	}
	
	// delete property
	@DeleteMapping("/deleteProperty/{id}")
	public ResponseEntity<ApiResponse<String>> deletePropertyById(@PathVariable("id") Integer id) {
		propertyService.deletePropertyById(id);		

		ApiResponse<String> response;
		response = new ApiResponse<>("Success", "Property Deleted Successfully");
    	return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	// Approve Property
	@PostMapping("/approveProperty/{id}")
	public ResponseEntity<ApiResponse<String>> approveProperty(@PathVariable("id") Integer id) {
		Optional<Property> property = propertyService.getPropertyById(id);
		Property p = property.get();
		p.setStatus(PropertyStatus.APPROVED);
		propertyService.saveProperty(p);
		
		ApiResponse<String> response;
		response = new ApiResponse<>("Success", "Property Approved successfully.");
    	return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
}
