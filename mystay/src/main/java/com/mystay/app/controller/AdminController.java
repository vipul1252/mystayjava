package com.mystay.app.controller;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.Roles;
import com.mystay.app.exception.UserAlreadyExistsException;
import com.mystay.app.repository.UserRepository;
import com.mystay.app.service.AuthenticationService;
import com.mystay.app.service.UserService;

import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin")
@RequiredArgsConstructor
public class AdminController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AuthenticationService authenticationService;

	
	@GetMapping
	public String admin() {
		return "Helllo Admin !!!";
	}
	
	@PostMapping("/addVerifier")
	public ResponseEntity<String> addUser(@RequestBody UserDTO user) {
		
		try {
			user.setRole(Roles.verifier);
			authenticationService.signup(user);
            return new ResponseEntity<>(Roles.verifier + " User added successfully.", HttpStatus.CREATED);
        } catch (UserAlreadyExistsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } 
	}
	
	// Get all Verifiers
	@GetMapping("/getAllVerifiers")
	public ResponseEntity<List<UserDTO>> getAllVerifiers() {
		
		List<UserDTO> verifiers = new ArrayList<>();
		verifiers = userService.getUsersListByRoles(Roles.verifier);
		
		return new ResponseEntity<>(verifiers, HttpStatus.OK);
        
	}
	
	// Get All Owners
	@GetMapping("/getAllOwners")
	public ResponseEntity<List<UserDTO>> getAllOwners() {
		
		List<UserDTO> owners = new ArrayList<>();
		owners = userService.getUsersListByRoles(Roles.owner);
		
		return new ResponseEntity<>(owners, HttpStatus.OK);
        
	}
	
	// Approve Property
	
	// Get All Properties
	
	// Get Approved Properties
	
	// Get Verified Properties

	// Total count of owners
	
	// Total count of verifiers
	
	// Total count of customers/users
	
	// Total count of unverified Properties
	
	// Total count of verified Properties
	
	// Total count of Approved Properties
	
	
	
}
