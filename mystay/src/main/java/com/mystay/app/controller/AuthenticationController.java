package com.mystay.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mystay.app.dto.ApiResponse;
import com.mystay.app.dto.JwtAuthenticationResponse;
import com.mystay.app.dto.RefreshTokenRequest;
import com.mystay.app.dto.SignInRequest;
import com.mystay.app.dto.SignUpRequest;
import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.Roles;
import com.mystay.app.exception.UserAlreadyExistsException;
import com.mystay.app.service.AuthenticationService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4000") // 
public class AuthenticationController {
	
	@Autowired
	private AuthenticationService authenticationService;
	
	@PostMapping("/signup")
	public ResponseEntity<String> signup(@RequestBody UserDTO user){
		user.setRole(Roles.user);
		try {
			user.setRole(Roles.user);
			authenticationService.signup(user);
            return new ResponseEntity<>("User added successfully.", HttpStatus.CREATED);
        } catch (UserAlreadyExistsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } 
	}

	@PostMapping("/signin")
	public ResponseEntity<ApiResponse<JwtAuthenticationResponse>> signin(@RequestBody SignInRequest signInRequest){
		
		JwtAuthenticationResponse jwtAuthenticationResponse = authenticationService.signin(signInRequest);
		
		ApiResponse<JwtAuthenticationResponse> response;
		response = new ApiResponse<>("success", jwtAuthenticationResponse);
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/refresh")
	public ResponseEntity<JwtAuthenticationResponse> refresh(@RequestBody RefreshTokenRequest refreshTokenRequest){
		return ResponseEntity.ok(authenticationService.refreshToken(refreshTokenRequest));
	}
	
}
