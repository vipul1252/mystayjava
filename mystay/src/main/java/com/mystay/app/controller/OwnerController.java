package com.mystay.app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mystay.app.dto.ApiResponse;
import com.mystay.app.entities.Property;
import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.PropertyAvailablity;
import com.mystay.app.enums.PropertyStatus;
import com.mystay.app.enums.Roles;
import com.mystay.app.exception.UserAlreadyExistsException;
import com.mystay.app.service.AuthenticationService;
import com.mystay.app.service.PropertyService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/owner")
@RequiredArgsConstructor
public class OwnerController {

	@Autowired
	private AuthenticationService authenticationService;
	
	@Autowired
	private PropertyService propertyService;
	
	// owner signup
	@PostMapping("/signup")
	public ResponseEntity<ApiResponse<String>> ownerSignUp(@RequestBody UserDTO user) {
		
		ApiResponse<String> response;

		try {
			user.setRole(Roles.owner);
			authenticationService.signup(user);
    		response = new ApiResponse<>("Success", Roles.owner + "  Added successfully.");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (UserAlreadyExistsException e) {
        	response = new ApiResponse<>("error", e.getMessage());
        	return new ResponseEntity<>(response, HttpStatus.CONFLICT);
        } catch (Exception e) {
        	response = new ApiResponse<>("error", e.getMessage());
        	return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } 
	}
	
	public String findUsername() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
		return username;
	}
	
	// Get Count of Properties of Owner
	@GetMapping("/propCount")
	public ResponseEntity<ApiResponse<Integer>> getCountOfOwnersProperties() {
		
		String username = findUsername();
        Optional<List<Property>> optionalProperties = propertyService.getAllOwnersProperties(username);
        List<Property> properties = optionalProperties.get();
        
        ApiResponse<Integer> response;
		response = new ApiResponse<>("Success", properties.size());
        return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	// Get Count of Rented Properties of Owner
	// Get Count of Properties of Owner
	@GetMapping("/rentedPropCnt")
	public ResponseEntity<ApiResponse<Integer>> getCountOfOwnersRentedProperties() {
		
		String username = findUsername();
		List<Property> properties = propertyService.getAllOwnersPropertiesByAvailability(username, PropertyAvailablity.RENTED).get();
		
        ApiResponse<Integer> response;
		response = new ApiResponse<>("Success", properties.size());
        return new ResponseEntity<>(response, HttpStatus.OK);
        
	}
	
	// Get Count of AVAILABLE Properties of Owner
	@GetMapping("/availablePropCnt")
	public ResponseEntity<ApiResponse<Integer>> getCountOfOwnersAvailableProperties() {
		String username = findUsername();
		List<Property> properties = propertyService.getAllOwnersPropertiesByAvailability(username, PropertyAvailablity.AVAILABLE).get();
        ApiResponse<Integer> response;
		response = new ApiResponse<>("Success", properties.size());
        return new ResponseEntity<>(response, HttpStatus.OK);
	}

	// Get Count of Approved Properties of Owner
	@GetMapping("/approvedPropCnt")
	public ResponseEntity<ApiResponse<Integer>> getCountOfOwnersApprovedProperties() {
		String username = findUsername();
		List<Property> properties = propertyService.getAllOwnersPropertiesByStatus(username, PropertyStatus.APPROVED).get();
        ApiResponse<Integer> response;
		response = new ApiResponse<>("Success", properties.size());
        return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	// Get Count of noOfOwnersAppointments
	
	// Get Count of noOfOwnersBookings
	
	// getAppointments
	
	// getBookings

}
