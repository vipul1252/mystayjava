package com.mystay.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mystay.app.entities.Property;
import com.mystay.app.enums.PropertyAvailablity;
import com.mystay.app.enums.PropertyStatus;


@Service
public interface PropertyService {

	Property saveProperty(Property property);

	Optional<Property> getPropertyById(Integer id);

	void deletePropertyById(Integer id);

	Optional<List<Property>> getAllOwnersProperties(String ownerName);

	Optional<List<Property>> getAllOwnersPropertiesByAvailability(String ownerName, PropertyAvailablity availability);
	
	Optional<List<Property>> getAllOwnersPropertiesByStatus(String ownerName, PropertyStatus status);
	
}
