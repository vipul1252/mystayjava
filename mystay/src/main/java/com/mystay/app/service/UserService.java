package com.mystay.app.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.mystay.app.dto.*;
import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.Roles;
import com.mystay.app.exception.UserAlreadyExistsException;

@Service
public interface UserService{
	public UserDTO getUserByID(Integer Id);
	public List<UserDTO> getUsersListByRoles(Roles role);
	public UserDetailsService userDetailsService();
	UserDTO getUserByUserName(String userName);
}
