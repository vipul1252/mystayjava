package com.mystay.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.Roles;
import com.mystay.app.exception.UserAlreadyExistsException;
import com.mystay.app.repository.UserRepository;
import com.mystay.app.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepo;
	
	
	@Override
	public UserDTO getUserByID(Integer Id) {
		return userRepo.findById(Id).get();
	}

	@Override
	public UserDTO getUserByUserName(String userName) {
		return userRepo.findByUserName(userName).get();
	}
	
	@Override
	public UserDetailsService userDetailsService() {
		return new UserDetailsService() {			
			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				return userRepo.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User Not Found"));
			}
		};
	}

	@Override
	public List<UserDTO> getUsersListByRoles(Roles role) {
		return userRepo.findByRole(role);
	}
	
}
