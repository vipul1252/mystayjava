package com.mystay.app.service;

import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public interface JWTService {
	String generateToken(UserDetails userDetails);
	String generateRefreshToken(Map<String,Object> extraClaims, UserDetails userDetails);
	String extractUserName(String token);
	String extractToken(String userDetails);
	public boolean isTokenValid(String token, UserDetails userDetails);
}
