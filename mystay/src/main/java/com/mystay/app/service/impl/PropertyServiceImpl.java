package com.mystay.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mystay.app.entities.Property;
import com.mystay.app.enums.PropertyAvailablity;
import com.mystay.app.repository.PropertyRepository;
import com.mystay.app.service.PropertyService;
import com.mystay.app.enums.PropertyStatus;


@Service
public class PropertyServiceImpl implements PropertyService{

	@Autowired
	private PropertyRepository propertyRepo;

	
	@Override
	public Property saveProperty(Property property) {
		return propertyRepo.save(property);
	}


	@Override
	public Optional<Property> getPropertyById(Integer id) {
		return propertyRepo.findById(id);
	}
	
	@Override
	public Optional<List<Property>> getAllOwnersProperties(String ownerName) {
		return propertyRepo.findByOwnerName(ownerName);
	}
	
	@Override
	public Optional<List<Property>> getAllOwnersPropertiesByAvailability(String ownerName, PropertyAvailablity availability) {
		return propertyRepo.findByOwnerNameAndAvailability(ownerName, availability);
	}
	
	@Override
	public void deletePropertyById(Integer id) {
		propertyRepo.deleteById(id);
	}


	@Override
	public Optional<List<Property>> getAllOwnersPropertiesByStatus(String ownerName, PropertyStatus status) {
		return propertyRepo.findByOwnerNameAndStatus(ownerName, status);
	}

}
