package com.mystay.app.service;

import org.springframework.web.bind.annotation.RequestBody;

import com.mystay.app.dto.JwtAuthenticationResponse;
import com.mystay.app.dto.RefreshTokenRequest;
import com.mystay.app.dto.SignInRequest;
import com.mystay.app.dto.SignUpRequest;
import com.mystay.app.entities.UserDTO;
import com.mystay.app.exception.UserAlreadyExistsException;

public interface AuthenticationService {
	UserDTO signup(UserDTO signUpRequest) throws UserAlreadyExistsException;
	JwtAuthenticationResponse refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest);
	JwtAuthenticationResponse signin(@RequestBody SignInRequest signInRequest);
}
