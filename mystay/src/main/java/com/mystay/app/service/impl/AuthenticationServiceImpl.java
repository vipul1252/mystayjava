package com.mystay.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.mystay.app.dto.JwtAuthenticationResponse;
import com.mystay.app.dto.RefreshTokenRequest;
import com.mystay.app.dto.SignInRequest;
import com.mystay.app.dto.SignUpRequest;
import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.Roles;
import com.mystay.app.exception.UserAlreadyExistsException;
import com.mystay.app.repository.UserRepository;
import com.mystay.app.service.AuthenticationService;
import com.mystay.app.service.JWTService;

import lombok.RequiredArgsConstructor;
import java.util.HashMap;


@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JWTService jwtService;
	
	
	@Override
	public UserDTO signup(UserDTO signUpRequest) throws UserAlreadyExistsException {
		UserDTO user = new UserDTO();
		
		user.setEmail(signUpRequest.getEmail());
		user.setFirstName(signUpRequest.getFirstName());
		user.setLastName(signUpRequest.getLastName());
		user.setRole(signUpRequest.getRole());
		user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));		
		
		try {
			if(userRepository.findByEmail(user.getEmail()).isPresent()) {
				user.setPassword(passwordEncoder.encode(user.getPassword()));
				throw new UserAlreadyExistsException("User with email " + user.getEmail() + " already exists.");
			}else {
				userRepository.save(user);
			}
		} finally {
			
		}
		return userRepository.save(user);
	}

	@Override
	public JwtAuthenticationResponse signin(@RequestBody SignInRequest signInRequest) {
		
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signInRequest.getEmail(), 
				signInRequest.getPassword()));
		
		UserDTO user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow(() -> new IllegalArgumentException("Invalid email or password"));
		String jwt = jwtService.generateToken(user);
		
		String refreshToken = jwtService.generateRefreshToken(new HashMap<>(),user);
		
		JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse();
		jwtAuthenticationResponse.setToken(jwt);
		jwtAuthenticationResponse.setEmail(user.getEmail());
		jwtAuthenticationResponse.setPhone(user.getPhone());
		jwtAuthenticationResponse.setFirstName(user.getFirstName());
		jwtAuthenticationResponse.setFirstName(user.getLastName());
		jwtAuthenticationResponse.setRole("owner");
		
		
//		jwtAuthenticationResponse.setRefreseToken(refreshToken);
		
		return jwtAuthenticationResponse;
	}
	
	@Override
	public JwtAuthenticationResponse refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest) {

		String userEmail = jwtService.extractUserName(refreshTokenRequest.getToken());
		
		UserDTO user = userRepository.findByEmail(userEmail).orElseThrow();
		
		if(jwtService.isTokenValid(refreshTokenRequest.getToken(), user)) {
			String jwt = jwtService.generateToken(user);
			
			JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse();

			jwtAuthenticationResponse.setToken(jwt);
//			jwtAuthenticationResponse.setRefreseToken(refreshTokenRequest.getToken());
			
			return jwtAuthenticationResponse;
		}
		
		return null;
	}
	
	
}
