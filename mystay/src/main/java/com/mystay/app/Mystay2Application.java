package com.mystay.app;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.Roles;
import com.mystay.app.repository.UserRepository;

@SpringBootApplication
public class Mystay2Application implements CommandLineRunner{

	@Autowired
	private UserRepository userRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(Mystay2Application.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

		List<UserDTO> adminAccount = userRepository.findByRole(Roles.admin);
		
		if (adminAccount == null) {
			UserDTO user = new UserDTO();
			user.setEmail("admin@gmail.com");
			user.setFirstName("admin");
			user.setLastName("admin");
			user.setRole(Roles.admin);
			
			user.setPassword(new BCryptPasswordEncoder().encode("admin"));
			
			userRepository.save(user);
			
		}
	}

	
}
