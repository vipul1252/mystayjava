package com.mystay.app.enums;

public enum PropertyStatus {
	VERIFICATION_PENDING("verficatonPending"),
	VERIFIED("verified"),
	APPROVED("approved");
	
	String value;
	
	PropertyStatus(String value){
		this.value = value;
	}
}
