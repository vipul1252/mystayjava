package com.mystay.app.enums;

public enum PropertyAvailablity {
	AVAILABLE("available"),
	RENTED("rented");
	
	String value;
	
	PropertyAvailablity(String value){
		this.value = value;
	}
}
