package com.mystay.app.enums;

public enum Roles {
	user("user"),
	admin("admin"),
	owner("owner"),
	verifier("verifier");
	
	String value;
	
	Roles(String value){
		this.value = value;
	}
	
}