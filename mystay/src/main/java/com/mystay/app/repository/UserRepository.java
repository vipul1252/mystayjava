package com.mystay.app.repository;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;
import org.springframework.stereotype.Repository;

import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.Roles;

@Repository
public interface UserRepository extends JpaRepository<UserDTO, Integer>{
	
	Optional<UserDTO> findByEmail(String email);
	
	Optional<UserDTO> findByUserName(String userName);
	
	List<UserDTO> findByRole(Roles role);
		
}
