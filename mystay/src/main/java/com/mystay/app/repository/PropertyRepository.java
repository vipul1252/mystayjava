package com.mystay.app.repository;

import org.springframework.stereotype.Repository;
import com.mystay.app.entities.Property;
import com.mystay.app.entities.UserDTO;
import com.mystay.app.enums.PropertyAvailablity;
import com.mystay.app.enums.PropertyStatus;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface PropertyRepository extends JpaRepository<Property, Integer>{
	Optional<List<Property>> findByOwnerName(String ownerName);
	Optional<List<Property>> findByOwnerNameAndAvailability(String ownerName, PropertyAvailablity availability);
	Optional<List<Property>> findByOwnerNameAndStatus(String ownerName, PropertyStatus status);
}
